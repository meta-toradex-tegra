FILESEXTRAPATHS:prepend := "${THISDIR}//files:"
DEPENDS:append:apalis-tk1 = " cbootimage-native"

SRC_URI:append:apalis-tk1 = " \
    file://apalis-tk1.img.cfg \
    file://fw_env.config \
    file://PM375_Hynix_2GB_H5TC4G63AFR_RDA_924MHz.bct \
"

do_deploy:append:apalis-tk1() {
    cd ${DEPLOYDIR}
    cp ${WORKDIR}/PM375_Hynix_2GB_H5TC4G63AFR_RDA_924MHz.bct .
    cbootimage -s tegra124 ${WORKDIR}/apalis-tk1.img.cfg apalis-tk1.img
    rm PM375_Hynix_2GB_H5TC4G63AFR_RDA_924MHz.bct
}
