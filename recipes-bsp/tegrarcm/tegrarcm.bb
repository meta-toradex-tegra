SUMMARY = "TegraRCM"
DESCRIPTION = "Utility used to upload payloads to a NVIDIA Tegra based device in recovery mode (RCM)."
SECTION = "bootloader"
DEPENDS = "libusb1 libcryptopp"

LICENSE = "Proprietary"
LIC_FILES_CHKSUM = "file://LICENSE;md5=395fe5affb633ad84474e42989a8e5be"

BBCLASSEXTEND = "native nativesdk"

SRC_URI = " \
    git://github.com/NVIDIA/tegrarcm.git;branch=master;protocol=https \
    file://0001-makefile-specify-cryptopp-include-path-relative-to-s.patch \
"
SRC_URI:append:class-native = " \
    file://0001-configure.ac-link-crypotpp-as-a-static-library.patch \
    file://0001-cryptopp-use-relative-path-for-cryptopp-headers.patch \
"

SRCREV = "ec1eeac53420de6b34e814ebd28e92099b257487"
PV = "1.7+"

EXTRA_OEMAKE = 'PREFIX="${prefix}" LIBDIR="${libdir}"'

LDFLAGS:append = " -static-libstdc++"

EXTRA_OEMAKE:class-native = "CC='${CC}' CXX='${CXX}'"
EXTRA_OEMAKE:class-nativesdk = "CC='${CC}' CXX='${CXX}'"

S = "${WORKDIR}/git"

inherit autotools pkgconfig

