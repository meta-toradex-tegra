PACKAGECONFIG:append:tegra124 = " dri3 egl gles gallium gbm "
EXTRA_OECONF:append:tegra124 = " --enable-texture-float --without-dri-drivers --enable-glx --enable-osmesa --enable-debug"
DRIDRIVERS:tegra124 = " "
GALLIUMDRIVERS:tegra124 = "nouveau,tegra"

PACKAGE_ARCH:tegra124 = "${MACHINE_ARCH}"
